#![deny(clippy::all)]
#![allow(clippy::missing_safety_doc)]

mod cheats;
mod config;
mod csgo;
mod gui;
mod memory;
mod offsets;

use crate::{cheats::*, config::Config, csgo::player::LocalPlayer, gui::app::GuiVars, memory::structs::Process, offsets::Offsets};
use csgo::player::{EntityPlayer, Player};
use eframe::{egui, NativeOptions};
use std::{
    marker::PhantomData,
    sync::{Arc, Mutex},
    thread::{self, sleep},
    time::Duration,
};

#[derive(Clone, Debug)]
pub struct RemotePtr<'a, T> {
    address: usize,
    runtime: &'a Runtime,
    inner: PhantomData<T>,
}

impl<'a, T> RemotePtr<'a, T> {
    pub unsafe fn read(&self) -> T {
        self.runtime
            .process
            .read(self.address)
            .unwrap_or_else(|| panic!("Failed to read pointer: {:#X}", self.address))
    }
    pub unsafe fn write(&self, value: &T) -> bool {
        self.runtime.process.write(self.address as u32, value)
    }
    pub fn cast<R>(&self) -> RemotePtr<R> {
        RemotePtr {
            address: self.address,
            runtime: self.runtime,
            inner: PhantomData,
        }
    }
    pub fn add(&self, offset: usize) -> Self {
        Self {
            address: self.address + offset,
            ..*self
        }
    }
}

#[derive(Debug)]
pub struct Runtime {
    process: Process,
    client: usize,
    engine: usize,
    offsets: Offsets,
}

impl Runtime {
    pub fn get_address(&self, offset: usize, client: bool) -> usize {
        if client {
            self.client + offset
        } else {
            self.engine + offset
        }
    }
    pub unsafe fn get_local_player(&self) -> Option<LocalPlayer> {
        LocalPlayer::new(self)
    }
    pub fn get_netvar(&self, netvar: &'static str) -> usize {
        self.offsets.netvars[netvar]
    }
    pub fn get_signature(&self, signature: &'static str) -> usize {
        self.offsets.signatures[signature]
    }
    pub unsafe fn get_entities(&self) -> impl Iterator<Item=EntityPlayer> {
        (1..16).map(move |i| EntityPlayer::get(self, i))
            .flatten()
            .filter(|entity| entity.is_alive())
    }
    pub unsafe fn read_ptr<T>(&self, offset: usize, client: bool) -> Option<RemotePtr<T>> {
        let address: usize = self
            .read_offset(offset, client)
            .expect(&*format!("Failed to read Pointer {:#X}", offset));

        if address == 0 {
            None
        } else {
            Some(RemotePtr {
                address,
                runtime: self,
                inner: PhantomData,
            })
        }
    }
    #[inline]
    pub unsafe fn read_offset<T>(&self, offset: usize, client: bool) -> Option<T> {
        let address = self.get_address(offset, client);
        self.process.read(address)
    }
    #[inline]
    pub unsafe fn write_offset<T>(&self, offset: usize, value: &T, client: bool) {
        let address = self.get_address(offset, client);
        self.process.write(address as u32, value);
    }
}

fn main() {
    let process = Process::from_name("csgo.exe").expect("Couldn't find cs:go process");
    println!("Found cs:go process with id {}", process.id);

    let client = process.get_module("client.dll").unwrap();
    let engine = process.get_module("engine.dll").unwrap();

    let offsets: Offsets = toml::from_str(include_str!("offsets.toml")).unwrap();

    let runtime = Runtime {
        process,
        client: client.base,
        engine: engine.base,
        offsets,
    };

    let variables = Arc::new(Mutex::new(GuiVars::default()));
    let config = Arc::new(Mutex::new(Config::load_from_file().unwrap_or_default()));

    let app = gui::app::GuiApp {
        variables: Arc::clone(&variables),
        config: Arc::clone(&config),
    };

    let options = NativeOptions {
        initial_window_size: Some(egui::Vec2 {
            x: 200f32,
            y: 238f32,
        }),
        ..Default::default()
    };
    thread::spawn(move || eframe::run_native(Box::new(app), options));

    let mut cheats = Vec::<Box<dyn HackModule>>::with_capacity(2);

    cheats.push(Box::new(BHop::new(config.clone())));
    cheats.push(Box::new(TriggerBot::new(config.clone())));
    cheats.push(Box::new(Fov::new(config.clone())));
    cheats.push(Box::new(Radar::new(config)));
    //cheats.push(Box::new(Glow::new(config)));

    unsafe {
        loop {
            if let Some(player) = runtime.get_local_player() {
                for cheat in &mut cheats {
                    cheat.execute(&player);
                }
            }
            sleep(Duration::from_millis(1));
        }
    }
}
