use std::sync::{Arc, Mutex};
use crate::{
    cheats::HackModule,
    config::Config,
    cheat,
    csgo::player::{EntityPlayer, LocalPlayer, Player},
};

cheat!(TriggerBot);

unsafe impl HackModule for TriggerBot {
    unsafe fn execute(&mut self, player: &LocalPlayer) {
        if self.config.lock().unwrap().trigger_enabled {
            if let Some(enemy) =
                EntityPlayer::get(player.get_runtime(), player.get_crosshair_id() - 1)
            {
                if player.get_team() != enemy.get_team() && enemy.is_alive() && !enemy.is_immune() && player.get_active_weapon_index().unwrap() != 42 {
                    player.force_attack()
                }
            }
        }
    }
}