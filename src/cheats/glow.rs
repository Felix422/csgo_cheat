#![allow(dead_code)]
use std::sync::{Arc, Mutex};
use crate::{cheat, config::Config, csgo::player::{EntityPlayer, Player}};
use super::HackModule;

cheat!(Glow);

unsafe impl HackModule for Glow {
    unsafe fn execute(&mut self, player: &crate::csgo::player::LocalPlayer) {
        for i in 1..64 {
            if let Some(current_player) = EntityPlayer::get(player.get_runtime(), i) {
                if !current_player.is_dormant() || !current_player.is_alive() {
                    if current_player.get_team() != player.get_team() {
                        // do the glow thing for enemies
                    } else {
                        // do the glow thing for teammates
                    }
                }
            }
        }
    }
}