use crate::{cheat, cheats::HackModule, csgo::player::{LocalPlayer, Player}, config::Config};
use std::sync::{Arc, Mutex};

cheat!(Radar);

unsafe impl HackModule for Radar {
    unsafe fn execute(&mut self, player: &LocalPlayer) {
        if self.config.lock().unwrap().radar_enabled {
            for enemy in player.get_runtime().get_entities() {
                enemy.set_spotted(true);
            }
        }
    }
}