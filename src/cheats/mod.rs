use crate::csgo::player::LocalPlayer;

pub mod bhop;
pub mod triggerbot;
pub mod fov;
pub mod radar;

pub use bhop::*;
pub use triggerbot::*;
pub use fov::*;
pub use radar::*;

#[macro_export]
macro_rules! cheat {
    ($name:ident) => {
        pub struct $name {
            config: Arc<Mutex<Config>>,
        }
        impl $name {
            pub fn new(config: Arc<Mutex<Config>>) -> Self {
                Self{config}
            }
        }
    };
    ($name:ident { $($field:ident: $type:ty = $value:expr),* }) => {
        pub struct $name {
            config: Arc<Mutex<Config>>,
            $($field: $type),*
        }
        impl $name {
            pub fn new(config: Arc<Mutex<Config>>) -> Self {
                Self{
                    config,
                    $($field: $value),*
                }
            }
        }
    }
}

pub unsafe trait HackModule {
    unsafe fn execute(&mut self, player: &LocalPlayer);
}
