#![allow(dead_code, unused_variables)]

use std::sync::{Arc, Mutex};
use crate::{cheats::HackModule, config::Config, csgo::player::{LocalPlayer, Player}};
use crate::cheat;

cheat!(Fov{
    is_disabled: bool = false
});

unsafe impl HackModule for Fov {
    unsafe fn execute(&mut self, player: &LocalPlayer) {
        let config = self.config.lock().unwrap();
        if config.fov_enabled {
            self.is_disabled = false;
            let fov = player.get_fov();
            if fov != config.fov {
                player.set_fov(config.fov);
            }
        } else if !self.is_disabled {
            self.is_disabled = true;
            player.set_fov(90);
            println!("Setting fov to default");
        }
    }
}