use crate::{
    cheats::HackModule,
    config::Config,
    cheat,
    csgo::player::{LocalPlayer, Player},
};
use std::sync::{Arc, Mutex};
use winapi::um::winuser::{GetAsyncKeyState, VK_SPACE};

cheat!(BHop);

unsafe impl HackModule for BHop {
    unsafe fn execute(&mut self, player: &LocalPlayer) {
        // reduce useless winapi calls
        #[allow(clippy::collapsible_if)]
        if self.config.lock().unwrap().bhop_enabled {
            if GetAsyncKeyState(VK_SPACE) != 0 {
                if player.is_on_ground() {
                    player.force_jump();
                }
            }
        }
    }
}
