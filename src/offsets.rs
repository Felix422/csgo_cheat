use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub struct Offsets {
    timestamp: i64,
    pub netvars: HashMap<String, usize>,
    pub signatures: HashMap<String, usize>,
}
