use crate::config::Config;
use eframe::{egui, epi};
use std::sync::{Arc, Mutex};

pub struct GuiVars {
    pub weapon_id: usize
}

impl Default for GuiVars {
    fn default() -> Self {
        Self {
            weapon_id: 0
        }
    }
}

pub struct GuiApp {
    pub variables: Arc<Mutex<GuiVars>>,
    pub config: Arc<Mutex<Config>>,
}

impl epi::App for GuiApp {
    fn name(&self) -> &'static str {
        "csgo_pog"
    }
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame) {
        // top menu bar
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                egui::menu::menu(ui, "Config", |ui| {
                    if ui.button("Save config").clicked() {
                        self.config.lock().unwrap().save_to_file();
                    }
                })
            });
        });
        // central panel
        egui::CentralPanel::default().show(ctx, |ui| {
            let vars = self.variables.lock().unwrap();
            ui.heading("Debug values");
            ui.label(format!("LocalPlayer's current weapon: {}", vars.weapon_id));
            ui.heading("Config");
            let mut config_lock = self.config.lock().unwrap();
            ui.checkbox(&mut config_lock.trigger_enabled, "Enable Trigger");
            ui.checkbox(&mut config_lock.bhop_enabled, "Enable BunnyHop");
            ui.checkbox(&mut config_lock.fov_enabled, "Enable FOV");
            ui.checkbox(&mut config_lock.force_fov, "Force FOV");
            ui.checkbox(&mut config_lock.radar_enabled, "Enable Radar");
            //ui.checkbox(&mut config_lock.glow_enabled, "Enable Glow");
            //ui.checkbox(&mut config_lock.glow_full_bloom, "Full Bloom");
            ui.add(egui::Slider::new(&mut config_lock.fov, 10..=160))
        });
    }
}
