use crate::{RemotePtr, Runtime};
use std::{thread::sleep, time::Duration};

pub unsafe trait Player<'a> {
    fn get_base_ptr(&self) -> RemotePtr<'a, usize>;

    fn add_netvar(&self, netvar: &'static str) -> RemotePtr<'a, usize> {
        self.get_base_ptr()
            .add(self.get_runtime().get_netvar(netvar))
    }
    #[inline]
    unsafe fn read_netvar<N>(&self, netvar: &'static str) -> N {
        self.add_netvar(netvar).cast().read()
    }
    #[inline]
    unsafe fn write_netvar<N>(&self, netvar: &'static str, value: &N) {
        self.add_netvar(netvar).cast().write(value);
    }

    #[inline]
    unsafe fn get_health(&self) -> usize {
        self.read_netvar("m_iHealth")
    }
    #[inline]
    unsafe fn get_team(&self) -> usize {
        self.read_netvar("m_iTeamNum")
    }
    #[inline]
    unsafe fn get_fov(&self) -> i32 {
        self.read_netvar("m_iDefaultFOV")
    }
    #[inline]
    unsafe fn get_flags(&self) -> usize {
        self.read_netvar("m_fFlags")
    }
    #[inline]
    unsafe fn get_crosshair_id(&self) -> i32 {
        self.read_netvar("m_iCrosshairId")
    }
    #[inline]
    unsafe fn get_glow_index(&self) -> i32 {
        self.read_netvar("m_iGlowIndex")
    }
    #[inline]
    unsafe fn is_dormant(&self) -> bool {
        self.read_netvar::<usize>("m_bDormant") == 1
    }
    #[inline]
    unsafe fn is_on_ground(&self) -> bool {
        self.get_flags() == 257
    }
    #[inline]
    unsafe fn is_alive(&self) -> bool {
        let health = self.get_health();
        (1..=100).contains(&health)
    }
    #[inline]
    unsafe fn is_immune(&self) -> bool {
        self.read_netvar("m_bGunGameImmunity")
    }
    #[inline]
    unsafe fn set_spotted(&self, spotted: bool) {
        self.write_netvar("m_bSpotted", &spotted);
    }
    #[inline]
    unsafe fn get_active_weapon_handle(&self) -> i32 {
        self.read_netvar("m_hActiveWeapon")
    }
    #[inline]
    unsafe fn get_active_weapon_entity(&self, weapon_handle: i32) -> Option<i32> {
        self.get_runtime().read_offset::<i32>(self.get_runtime().get_signature("dwEntityList") + (((weapon_handle & 0xFFF) - 1) * 0x10) as usize, true)
    }
    #[inline]
    unsafe fn get_active_weapon_index(&self) -> Option<usize> {
        let definition_index = self.get_runtime().get_netvar("m_iItemDefinitionIndex");
        let active_weapon_entity = self.get_active_weapon_entity(self.get_active_weapon_handle())? as usize;
        self.get_runtime().process.read(active_weapon_entity + definition_index)
    }
    #[inline]
    unsafe fn get_glow_object_manager(&self) -> RemotePtr<'a, usize> {
        self.get_runtime()
            .read_ptr::<usize>(self.get_runtime().get_signature("dwGlowObjectManager"), true)
            .unwrap()
    }

    fn get_runtime(&self) -> &'a Runtime;
}

pub struct LocalPlayer<'a> {
    runtime: &'a Runtime,
    inner: RemotePtr<'a, usize>,
}

#[derive(Debug)]
pub struct EntityPlayer<'a> {
    runtime: &'a Runtime,
    inner: RemotePtr<'a, usize>,
}

unsafe impl<'a> Player<'a> for LocalPlayer<'a> {
    fn get_base_ptr(&self) -> RemotePtr<'a, usize> {
        self.inner.clone()
    }

    fn get_runtime(&self) -> &'a Runtime {
        self.runtime
    }
}

unsafe impl<'a> Player<'a> for EntityPlayer<'a> {
    fn get_base_ptr(&self) -> RemotePtr<'a, usize> {
        self.inner.clone()
    }

    fn get_runtime(&self) -> &'a Runtime {
        self.runtime
    }
}

impl<'a> LocalPlayer<'a> {
    pub unsafe fn new(runtime: &'a Runtime) -> Option<Self> {
        let inner = runtime.read_ptr::<usize>(runtime.get_signature("dwLocalPlayer"), true)?;
        Some(LocalPlayer { runtime, inner })
    }
    #[inline]
    pub unsafe fn set_fov(&self, fov: i32) {
        self.write_netvar("m_iDefaultFOV", &fov);
    }
    #[inline]
    pub unsafe fn force_jump(&self) {
        self.runtime
            .write_offset(self.runtime.get_signature("dwForceJump"), &5, true);
        sleep(Duration::from_millis(1));
        self.runtime
            .write_offset(self.runtime.get_signature("dwForceJump"), &4, true);
    }
    #[inline]
    pub unsafe fn force_attack(&self) {
        self.runtime
            .write_offset(self.runtime.get_signature("dwForceAttack"), &5, true);
        sleep(Duration::from_millis(1));
        self.runtime
            .write_offset(self.runtime.get_signature("dwForceAttack"), &4, true);
    }
}

impl<'a> EntityPlayer<'a> {
    pub unsafe fn get(runtime: &'a Runtime, index: i32) -> Option<EntityPlayer<'a>> {
        if index <= 0 {
            return None;
        }
        let inner = runtime.read_ptr::<usize>(
            runtime.get_signature("dwEntityList") + (index as usize * 0x10),
            true,
        )?;
        Some(EntityPlayer { runtime, inner })
    }
}
