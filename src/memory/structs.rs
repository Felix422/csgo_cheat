use crate::memory::Constructor;
use std::{cell::RefCell, collections::HashMap, mem, ops::Deref, ptr, rc::Rc};
use winapi::{
    shared::{
        basetsd::SIZE_T,
        minwindef::{BOOL, FALSE, LPCVOID, LPVOID, PBOOL, TRUE},
        ntdef::HANDLE,
    },
    um::{
        handleapi::{CloseHandle, INVALID_HANDLE_VALUE},
        memoryapi::{ReadProcessMemory, WriteProcessMemory},
        processthreadsapi::OpenProcess,
        tlhelp32::{
            CreateToolhelp32Snapshot, Module32FirstW, Module32NextW, Process32FirstW,
            Process32NextW, MODULEENTRY32W, PROCESSENTRY32W, TH32CS_SNAPMODULE,
            TH32CS_SNAPMODULE32, TH32CS_SNAPPROCESS,
        },
        winnt::PROCESS_ALL_ACCESS,
        wow64apiset::IsWow64Process,
    },
};
#[derive(Debug, Clone)]
pub struct Module {
    pub name: String,
    pub base: usize,
    pub size: usize,
    pub data: Vec<u8>,
}
#[derive(Debug, Clone)]
pub struct Process {
    pub id: u32,
    pub is_32bit: bool,
    handle: HANDLE,
    modules: RefCell<HashMap<String, Rc<Module>>>,
}
pub struct SnapshotHandle {
    pub handle: HANDLE,
}

impl Constructor for MODULEENTRY32W {
    fn new() -> Self {
        let mut module: Self = unsafe { mem::zeroed() };
        module.dwSize = mem::size_of::<MODULEENTRY32W>() as u32;
        module
    }
}

impl Constructor for PROCESSENTRY32W {
    fn new() -> Self {
        let mut process_entry: PROCESSENTRY32W = unsafe { mem::zeroed() };
        process_entry.dwSize = mem::size_of::<PROCESSENTRY32W>() as u32;
        process_entry
    }
}

impl Module {
    fn from_module_entry(
        module_entry: MODULEENTRY32W,
        name: &str,
        process: &Process,
    ) -> Option<Rc<Self>> {
        let mut module = Self {
            name: name.to_string(),
            base: module_entry.modBaseAddr as usize,
            size: module_entry.modBaseSize as usize,
            data: vec![0u8; module_entry.modBaseSize as usize],
        };

        if process.read_ptr(module.data.as_mut_ptr(), module.base, module.size) {
            return Some(Rc::new(module));
        }

        None
    }
}

impl Process {
    pub fn read<T>(&self, address: usize) -> Option<T> {
        let mut buffer = unsafe { mem::zeroed::<T>() };
        match unsafe {
            ReadProcessMemory(
                self.handle,
                address as LPCVOID,
                &mut buffer as *mut T as LPVOID,
                mem::size_of::<T>() as SIZE_T,
                ptr::null_mut::<SIZE_T>(),
            )
        } {
            FALSE => None,
            _ => Some(buffer),
        }
    }
    pub fn write<T>(&self, address: u32, buf: &T) -> bool {
        unsafe {
            WriteProcessMemory(
                self.handle,
                address as LPVOID,
                buf as *const T as LPCVOID,
                mem::size_of::<T>() as SIZE_T,
                ptr::null_mut::<SIZE_T>(),
            ) != FALSE
        }
    }
    pub fn from_pid(pid: u32) -> Option<Self> {
        let handle = unsafe { OpenProcess(PROCESS_ALL_ACCESS, 0, pid) };
        if handle.is_null() {
            return None;
        }
        let mut tmp: BOOL = 0;

        if unsafe { IsWow64Process(handle, &mut tmp as PBOOL) } == FALSE {
            println!("Could not determine process bitness: IsWow64Process returned an error!");
            return None;
        }
        let is_32bit = !matches!(tmp, FALSE);
        Some(Self {
            id: pid,
            is_32bit,
            handle,
            modules: RefCell::new(HashMap::new()),
        })
    }
    pub fn from_name(name: &str) -> Option<Self> {
        let snapshot = SnapshotHandle::new(0, TH32CS_SNAPPROCESS)?;
        let mut process_entry = PROCESSENTRY32W::new();

        if unsafe { Process32FirstW(*snapshot, &mut process_entry) == FALSE } {
            return None;
        }

        loop {
            let process_name = String::from_utf16(&process_entry.szExeFile).unwrap_or_default();
            if process_name.contains(name) {
                return Process::from_pid(process_entry.th32ProcessID);
            }
            if unsafe { Process32NextW(*snapshot, &mut process_entry) == FALSE } {
                break;
            }
        }
        None
    }

    // Not done
    #[allow(dead_code, unused_mut, unused_variables)]
    pub fn get_module(&self, name: &str) -> Option<Rc<Module>> {
        let mut modules = self.modules.borrow_mut();
        if modules.contains_key(name) {
            return modules.get(name).cloned();
        }
        let snapshot = SnapshotHandle::new(self.id, TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32)?;
        let mut module_entry = MODULEENTRY32W::new();

        if unsafe { Module32FirstW(*snapshot, &mut module_entry) == FALSE } {
            return None;
        }

        loop {
            let module_name = String::from_utf16_lossy(&module_entry.szModule)
                .trim_matches('\0')
                .to_string();

            if module_name == name {
                return Module::from_module_entry(module_entry, &module_name, self);
            }

            if unsafe { Module32NextW(*snapshot, &mut module_entry) == FALSE } {
                break;
            }
        }
        None
    }

    pub fn read_ptr<T>(&self, buf: *mut T, addr: usize, count: usize) -> bool {
        unsafe {
            ReadProcessMemory(
                self.handle,
                addr as LPCVOID,
                buf as *mut T as LPVOID,
                mem::size_of::<T>() as SIZE_T * count,
                ptr::null_mut::<SIZE_T>(),
            ) == TRUE
        }
    }
}

impl Drop for Process {
    fn drop(&mut self) {
        if !self.handle.is_null() {
            unsafe { CloseHandle(self.handle) };
        }
    }
}

impl SnapshotHandle {
    #[allow(dead_code)]
    pub fn new(pid: u32, flags: u32) -> Option<Self> {
        let handle = unsafe { CreateToolhelp32Snapshot(flags, pid) };
        if handle.is_null() || handle == INVALID_HANDLE_VALUE {
            return None;
        }
        Some(SnapshotHandle { handle })
    }
}

impl Drop for SnapshotHandle {
    fn drop(&mut self) {
        unsafe {
            CloseHandle(self.handle);
        }
    }
}

impl Deref for SnapshotHandle {
    type Target = HANDLE;
    fn deref(&self) -> &HANDLE {
        &self.handle
    }
}
