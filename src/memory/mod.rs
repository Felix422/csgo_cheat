pub mod structs;

trait Constructor {
    fn new() -> Self;
}
