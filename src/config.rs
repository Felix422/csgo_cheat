use std::{
    fs::{create_dir, File, OpenOptions},
    io::{Read, Write},
};
use eframe::egui::Color32;

#[derive(Clone, Copy, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Config {
    pub bhop_enabled: bool,
    pub trigger_enabled: bool,
    pub fov_enabled: bool,
    pub glow_enabled: bool,
    pub radar_enabled: bool,
    pub force_fov: bool,
    pub fov: i32,
    #[serde(with = "Color32Def")]
    pub inactive_glow_color: Color32,
    #[serde(with = "Color32Def")]
    pub enemy_glow_color: Color32,
    #[serde(with = "Color32Def")]
    pub team_glow_color: Color32,
    pub glow_full_bloom: bool
}

impl Default for Config {
    fn default() -> Self {
        Self {
            bhop_enabled: false,
            trigger_enabled: false,
            fov_enabled: false,
            glow_enabled: false,
            radar_enabled: false,
            force_fov: false,
            fov: 90,
            inactive_glow_color: Color32::from_rgb(0, 0, 255),
            enemy_glow_color: Color32::from_rgb(255, 0, 0),
            team_glow_color: Color32::from_rgb(0, 255, 0),
            glow_full_bloom: false       
        }
    }
}

impl Config {
    pub fn load_from_file() -> Option<Self> {
        let config_dir = dirs::home_dir().unwrap().join(".csgo_pog");
        if !config_dir.exists() || !config_dir.join("config.toml").exists() {
            return None;
        }
        let mut file_buf = String::new();
        let mut file = File::open(config_dir.join("config.toml")).unwrap();
        let _ = file.read_to_string(&mut file_buf);
        Some(toml::from_str(&file_buf).unwrap())
    }

    pub fn save_to_file(&self) {
        let config_dir = dirs::home_dir().unwrap().join(".csgo_pog");
        if !config_dir.exists() {
            create_dir(&config_dir).unwrap();
        }
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(config_dir.join("config.toml"))
            .unwrap();
        let _ = file.write_all(toml::to_string(self).unwrap().as_bytes());
    }
}

#[derive(serde::Serialize, serde::Deserialize)]
#[serde(remote = "Color32")]
pub struct Color32Def([u8; 4]);
